* ansible vault
* hostvars, groupvars
* wszystko przepisać jako rola common-linux
* dopisać handlery do restartów maszyny i usług i firewali (tak jak egzaminowe)
* wheny gdy należy do danej grupy
* dynamiczny inventory (może kiedyś)
* automatycznie generowany etc/hosts i jakiś plik z listą serwerów (jak egzaminowe podobne)
* naprawić task na restart kompa async
* ogarnąć tą zmienną dla networku statycznego
* korzystać z rhel-system-roles
* napisać role do:
    * jenkins server
    * jenkins agent
    * lamp
    * gemini server
    * nfs server
    * samba server
    * nfs/samba klient
    * ftp server
    * ftp client
    * rsyslog server
    * rsyslog client
    * backup server
    * ansible controler (ansible, python, autocompletion, vim nano, vim konfiguracja, rhel-system-role)
    * backup skonfigurowany?
    * nodejs server z pm2
    * docker instalacja + swarm + docker compose
    * cockpit server
    * debian-open-shell server (osobne notatki mam pod to)
    * ha proxy dla lampa, może ngix?
    * ha? filesytemowy? czy coś?
    * healthcheck
    * QA, quality gate, lynis, clamav i może ten knight coś tam , audyt security ogólnie
    * ntpd server
    * ntpd client
    * dodanie usera
    * archiwizacja usera (usunięcie ale bezpieczne)
    * podmonotwanie filesystemu, odmontowanie
    * tworzenie filesystemów LVM i VDO?
    * monitoring server
    * monitoring agent
    * sposób na ogarnięcie na masterze logów? coś jak elastic search?
    * desktop preparation role
